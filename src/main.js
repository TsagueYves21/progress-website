import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import $loadScript from "./plugins/loadScript";

// import './assets/main.css'

const app = createApp(App)
app.config.globalProperties.$loadScript = $loadScript;

app.use(router)

app.mount('#app')
