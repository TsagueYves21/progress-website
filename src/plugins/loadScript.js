const $loadScript = (url) => {
    const tag = document.createElement('script');
    tag.src = url;
    document.body.append(tag);
}

export default $loadScript